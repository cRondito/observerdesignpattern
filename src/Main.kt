import kotlin.properties.Delegates

fun main(args: Array<String>) {
    val textView = TextView()
    textView.listener = PrintingTextChangedListener()
    println("No hay texto")
    textView.text = "Bueno ahora sí"
    textView.text = "Cambió otra vez el texto"
}

interface TextChangedListener {
    fun onTextChanged(newText: String)
}

class PrintingTextChangedListener : TextChangedListener {
    override fun onTextChanged(newText: String) = println("CAMBIO DE TEXTO: $newText")
}

class TextView {
    var listener: TextChangedListener? = null
    var text: String by Delegates.observable("") { prop, old, new ->
        listener?.onTextChanged(new)
    }
}